# BSTProb #

Open in Visual Studio 2013 or 2015.

This project takes a dictionary of tree nodes, each with a probability of being accessed, and uses a dynamic programming algorithm to arrange them into a least-cost binary tree based on those probabilities. Generally, the nodes with lowest probability will be farther down, and the highest probability will be the root node or on the second layer.